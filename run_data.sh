#!/usr/bin/env bash

export start="1973-01-01"
export end="2023-01-01"
export idstation="87765"  # Bariloche: 87765
export workdir=$PWD       # or path
export season="DJF"       # verano: "DJF", invierno:"JJA", otoño: "MAM", primavera: "SON"

# Si lo tengo, activo entorno de conda
# conda activate clonenewone

# descargo la informacion
Rscript $workdir/get_data.R 2>$workdir/logR.err >$workdir/logR.out

# proceso la informacion
python $workdir/warming_stripes.py 2>$workdir/logP.err >$workdir/logP.out
