#!/usr/bin/env phyton

import os, sys
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
from matplotlib.collections import PatchCollection
from matplotlib.colors import ListedColormap
import pandas as pd

# Data period
FIRST   = os.environ['start']
LAST    = os.environ['end']  # inclusive
WORKDIR = os.environ['workdir']
IDSTAT  = os.environ['idstation']
SEASON  = os.environ['season']

# Reference climat period
FIRST_REFERENCE = 1991
LAST_REFERENCE  = 2020

# data from script bariloche.r
name_file = f'{WORKDIR}/dataframe.csv'
dataset   = pd.read_csv(name_file, sep = ",", na_values="NA")

# Get season
df        = dataset[dataset.seas==SEASON].set_index("year")
# Subset data to compute anomalies
subset    = df.loc[FIRST:LAST, 't2m'].dropna()
reference = subset.loc[FIRST_REFERENCE:LAST_REFERENCE].mean()
# Get anomaly
df["t2ma"] = df["t2m"] - reference


# Define x-axis limits
start_year = df.index[0]
end_year   = df.index[-1]

# Create the plot
fig,ax = plt.subplots(figsize=(14, 4))

cmap = 'RdBu_r'

rect_coll = PatchCollection([Rectangle((y, 0), 1, 1) for y in range(start_year, end_year + 1)], zorder=2)
rect_coll.set_array(df.t2m)
rect_coll.set_cmap(cmap)
ax.add_collection(rect_coll)

ax.set_ylim(0, 1)
ax.set_xlim(start_year, end_year + 1)
ax.yaxis.set_visible(False)
ax.set_title(f"Estación Meteorológica {IDSTAT} - {SEASON}", fontsize=18, loc='left', y=1.03, fontweight='bold', color='k')

ax2 = ax.twinx()
ax2.plot(df.index + 0.5, df.t2m, color='black', linewidth=1.5, )
ax2.yaxis.tick_left()
ax2.yaxis.set_label_position('left')
ax2.set_ylabel(f'Temperatura media (°C)\n {SEASON}', fontsize=15)

ax3 = ax2.twinx()
ax3.set_ylim(ax2.get_ylim())
ax3.yaxis.set_visible(False)

coef = np.polyfit(df.index, df.t2m, 1)
trend = np.poly1d(coef)

ax3.plot(df.index + 0.5, trend(df.index + 0.5), linestyle='--', color='black', linewidth=1, )

plt.figtext(0.815, 0.89, 'Autor. Matias De Oto', fontsize=8)
plt.figtext(0.9, 0.14,  'Data source: RNOAA',  rotation=270, fontsize=8)

fig.savefig(f'{WORKDIR}/warming-stripes-{SEASON}.png')
