# Descarga de observaciones meteorologicas horarias desde la base de datos de la noaa y procesamiento de los mismos para generar un diagrama de Hawkins

### Requerimientos

   * Sistema operativo: Ubuntu/Linux
   * Lenguaje de programación: Python versión +3.8, R versión +4, shell
   * Librerías de python requeridas: numpy, pandas, matplotlib.
   * Librerias de R requeridas: rnoaa, seas

### Introducción

Los [Diagramas de Hawkins](https://www.reading.ac.uk/planet/climate-resources/climate-stripes) o "warming stripes" fueron desarrollados por el Prof. Ed Hawkins para dar una visión sencilla y minimalista del cambio temporal de la temperatura global. A continuación, se genera un guía con algunos programas sencillos para descargar la información de temperatura in-situ de estaciones meteorológicas y procesar las mismas para generar el gráfico de Hawkins.

### Repositorio de Datos

Los información meteorológica se encuentra en una resolución diaria dentro de la base de datos de la NOAA. Para acceder a la misma, **Chamberlain S (2022)** desarrolló una librería en R denominada ["rnoaa"](https://github.com/ropensci/rnoaa). En el link se encuentran los pasos para descargar la librería, se sugiere además leer el [manual](https://cran.r-project.org/web/packages/rnoaa/rnoaa.pdf)

### Descargar archivos del GIT

    $ `git clone https://gitlab.com/mdeotoproschle/warming_strings`

### Ejecución

Una vez chequeado que las librerias estén bien instaladas. Se recomienda crearse un entorno de conda con la version de Python y R sugeridas. Los procedimientos para generarse un entorno de trabajo conda se encuentran en el siguiente [tutorial](https://conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html#creating-an-environment-with-commands).

Desde la terminal de linux, posicionados en la carpeta donde se han descargado los archivos desde el git:

    0. Abrimos el archivo run_data.sh

            $ `vi run_data.sh`

    1. Se setean las variables de exportación

        Para escribir en vi, apretamos letra `i`,
          **start**: fecha inicial,
          **end**: fecha final de descarga de descarga de datos,
          **idstation**: id de de la estacion meteorológica Por ejemplo el id de Bariloche es **87765**. El **id** (indicativo synop) de la estación meteorologica puede consultarse en [(ogimet)](https://www.ogimet.com/indicativos.phtml). Para comprobar si existe información de otras estaciones meteorológicas, se recomienda utilizar la función **meteo_nearby_stations()** de la librería rnoaa.
          **workdir**: path de carpeta de trabajo, se recomienda el directorio donde se encuentran los archivos.
          **season**: El trimestre utilizado para agrupar la información y realizar los promedios. Default: "DJF"
        Para guardar apretamos la tecla `ESC` y luego `:wq`.

    2. Damos permisos de ejecución al archivo run_data.sh

            $ `chmod a+x run_data.sh`

    3. Ejecutamos el archivo, mediante el comando en terminal

            $ `bash run_data.sh`

**ATENCION** La ejecución del archivo run_data.sh descarga la información meteorológica procesada estacionalmente en un archivo **.csv** . Este archivo luego es utilizado para generar el diagrama de Hawkins. Los errores de ejecución como las capturas de pantalla se guardan en los archivos log.err y log.out en el escritorio de ejecución.

### Agradecimientos

La contibución a este proyecto fue posible gracias a las contribuciones de Chamberlain S (2022) y de [Deutsches Klimarechentrum GmbH](https://docs.dkrz.de/doc/visualization/sw/python/source_code/python-matplotlib-warming-stripes-and-trend.html#dkrz-python-matplotlib-warming-stripes-and-trend)
